//Write a program to add two user input numbers using 4 functions.


#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter number\n");
    scanf("%d",&a);
    return a;
}

int compute(int a,int b)
{
    int sum;
    sum=a+b;
    return sum;
}

void display(int a,int b,int sum)
{
    printf("Sum of %d and %d is %d",a,b,sum);
}


int main()
{
    int a,b,Sum;
    a=input();
    b=input();
    Sum=compute(a,b);
    display(a,b,Sum);
    return 0;
}