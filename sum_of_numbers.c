//Write a program to find the sum of n different numbers using 4 functions

//sum of two numbers

#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter number:");
    scanf("%d",&a);
    return a;
}

int compute(int a,int b)
{
    int sum;
    sum=a+b;
    return sum;
}

void display(int sum)
{
    printf("Sum is %d",sum);
}


int main()
{
    int a,b,Sum;
    a=input();
    b=input();
    Sum=compute(a,b);
    display(Sum);
    return 0;
}