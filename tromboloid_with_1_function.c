//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
    float h,d,b,vol;
    printf("Enter values for h,d,b:\n");
    scanf("%f %f %f",&h,&d,&b);
    
    vol=((h*d)+d)/(3*b);
    
    printf("Volume of tromboloid = %0.2f\n",vol);
    
    return 0;
}