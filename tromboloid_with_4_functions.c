//WAP to find the volume of a tromboloid using 4 functions

#include<stdio.h>
float input(char x)
{
    float a;
    printf("Enter value of %c:\n",x);
    scanf("%f",&a);
    return a;
}

float vol(float h,float d,float b)
{
    float vlm=((h*d)+d)/(3*b);
    return vlm;
}

void output(float vlm)
{
    printf("Volume of tromboloid is %0.2f",vlm);
}

int main()
{
   float h,d,b,vlm;
   
   h=input('h');
   d=input('d');
   b=input('b');
   vlm=vol(h,d,b);
   output(vlm);
   return 0; 
}