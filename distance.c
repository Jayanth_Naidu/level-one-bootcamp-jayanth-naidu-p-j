#include <stdio.h>
#include<math.h>
float abs1()
{
    float a;
    printf("Enter abscissa for first point: ");
    scanf("%f",&a);
    return a;
}
float ord1()
{
    float a;
    printf("Enter ordinate for first point: ");
    scanf("%f",&a);
    return a;
}float abs2()
{
    float a;
    printf("Enter abscissa for second point: ");
    scanf("%f",&a);
    return a;
}float ord2()
{
    float a;
    printf("Enter ordinate for second point: ");
    scanf("%f",&a);
    return a;
}


float compute(float x1,float y1,float x2,float y2)
{
    float dist;
    dist=sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    return dist;
}

void output(float x1,float y1,float x2,float y2,float dist)
{
    printf("distance between (%0.1f,%0.1f) and (%0.1f,%0.1f) is %0.3f",x1,y1,x2,y2,dist);
    
}    
 
int main()
{
    float x1,x2,y1,y2;
    float d;
    x1=abs1();
    y1=ord1();
    x2=abs2();
    y2=ord2();
    d=compute(x1,y1,x2,y2);
    output(x1,y1,x2,y2,d);
    return 0;
}